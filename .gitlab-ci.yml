image: "salesforce/salesforcedx:latest-full"

cache:
  key: ${CI_COMMIT_REF_NAME}
  paths:
    - node_modules/
    - .sfdx/

stages:
  - commit-test
  - merge-request-test

scanner:
  stage: commit-test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH != "develop" && $CI_COMMIT_BRANCH != "master" && $CI_PIPELINE_SOURCE == "push"'
  script:
    - run_pmd

test-lwc:
  stage: commit-test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH != "develop" && $CI_COMMIT_BRANCH != "master" && $CI_PIPELINE_SOURCE == "push"'
  script:
    - install_lwc_jest
    - test_lwc_jest

test-apex:
  stage: merge-request-test
  only:
    - merge_requests
  script:
    - authenticate TEST_ORG $TEST_ORG_AUTH_URL
    - deploy_checkOnly TEST_ORG
  artifacts:
    reports:
      junit: tests/apex/*-junit.xml
    paths:
      - tests/
  coverage: '/name="testRunCoverage" value="([\d]+%)"/'

test-apex-after:
  stage: merge-request-test
  only:
    - develop
  script:
    - authenticate TEST_ORG $TEST_ORG_AUTH_URL
    - deploy_to_org TEST_ORG
    - test_apex
  artifacts:
    reports:
      junit: tests/apex/*-junit.xml
    paths:
      - tests/
  coverage: '/name="testRunCoverage" value="([\d]+%)"/'

.sfdx_helpers: &sfdx_helpers |

  function run_pmd() {

    sfdx scanner:run  --pmdconfig "ruleset.xml" -t force-app -f table
  }

  # Function to install LWC Jest dependencies.
  # Will create or update the package.json with { "test:lwc" : "lwc-jest" } to the scripts property.
  # No arguments.

  function install_lwc_jest() {
    echo 'inside install_lwc_jest'
    # Create a default package.json if file doesn't exist
    if [ ! -f "package.json" ]; then
      npm init -y
    fi

    # Check if the scripts property in package.json contains key for "test:lwc"
    local scriptValue=$(jq -r '.scripts["test:lwc"]' < package.json)

    # If no "test:lwc" script property, then add one
    if [[ -z "$scriptValue" || $scriptValue == null ]]; then
      local tmp=$(mktemp)
      jq '.scripts["test:lwc"]="lwc-jest"' package.json > $tmp
      mv $tmp package.json
      echo "added test:lwc script property to package.json" >&2
      cat package.json >&2
    fi

    # Now that we have package.json to store dependency references to
    # and to run our lwc jest test scripts, run npm installer
    npm install
    npm install @salesforce/lwc-jest --save-dev

  }


  # Checks if there are LWC Jest Test files in any of the package directories of sfdx-project.json.
  # This is necessary because npm will throw error if no test classes are found.
  # No arguments.
  # Returns `true` or `false`

  function check_has_jest_tests() {
    echo 'inside function check_has_jest_tests'
    local hasJestTests=false
    for pkgDir in $(jq -r '.packageDirectories[].path' < sfdx-project.json)
    do
      if [ -f $pkgDir ]; then
        local fileCnt=$(find $pkgDir -type f -path "**/__tests__/*.test.js" | wc -l);
        if [ $fileCnt -gt 0 ]; then
          hasJestTests=true
        fi
      fi
    done
    echo $hasJestTests
  }


  # Runs `npm run test:lwc` to execute LWC Jest tests.
  # Function takes no arguments.
  # Should be called after `setup_lwc`.
  # Uses `check_has_jest_tests` to know if there are actually any tests to run.
  # If there aren't any jest tests then npm would throw an error and fail the job,
  # so we skip running npm if there are no tests, essentially skipping them to avoid error.

  function test_lwc_jest() {
    echo 'inside function test_lwc_jest'
    local hasJestTests=$(check_has_jest_tests)
    if [ $hasJestTests ]; then
      npm cache clean --force
      npm install
      npm run test:lwc
    else
      echo 'Skipping lwc tests, found no jest tests in any package directories' >&2
    fi
  }

  function test_apex() {

    # Make directory to output test results
    # https://gitlab.com/help/ci/yaml/README.md#artifactsreports
    mkdir -p ./tests/apex

    sfdx force:apex:test:run -c -r junit -l RunLocalTests -w 20 -d ./tests/apex
  }

  function authenticate() {

    local alias_to_set=$1
    local org_auth_url=$2

    local file=$(mktemp)
    echo $org_auth_url > $file
    local cmd="sfdx force:auth:sfdxurl:store --sfdxurlfile $file --setalias $alias_to_set --json" && (echo $cmd >&2)
    local output=$($cmd)

    sfdx force:config:set defaultusername=$alias_to_set

    rm $file
  }

  function deploy_checkOnly() {
    local org_alias=$1
    
    sfdx force:source:deploy -p force-app -u ${org_alias} -c -l RunLocalTests --json
  }

  function deploy_to_org() {
    local org_alias=$1
    
    sfdx force:source:deploy -p force-app -u ${org_alias}
  }

before_script:
  - *sfdx_helpers
