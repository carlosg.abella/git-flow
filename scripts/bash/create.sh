readonly COPADO_PACKAGE='04t1t000003f1HnAAI'
devhub=$1
orgname=$(git branch --show-current)

if [ "$orgname" == "master" ] || [ "$orgname" == "main" ] || [ "$orgname" == "develop" ]; then
    echo "You can not create a ScratchOrg from a protected branch"
    exit
fi

if [ -z "$1" ]; then
    echo "Please type your devhub user name or alias"
    read inputdevhub
    devhub=$inputdevhub
fi

echo ""
echo "Creating Scratch Org ${orgname} ..."
sfdx force:org:create -v $devhub --wait 10 --durationdays 30 -a $orgname --definitionfile config/project-scratch-def.json orgName=$orgname || {
    (echo >&2 'Scratch Org creation has failed')
    exit 1
}

echo ""
echo "Installing Copado as dependency ..."
sfdx force:package:install -u ${orgname} -p ${COPADO_PACKAGE} -w 200 || {
    (echo >&2 'Copado package installation has failed')
    exit 1
}

echo ""
echo "Pushing Metadata to Scratch Org"
sfdx force:source:push -u ${orgname} || {
    (echo >&2 'Deploy Metadata to org has failed')
    exit 1
}
